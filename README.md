# nutie

> Information extraction intelligent and fun

![Coreference resolution](img/nutIE.png)

![Data viewer](img/nutIE_01.png)

![Model evaluation](img/nutIE_02.png)

![Coreference comparison](img/nutIE_03.png)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8888
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
