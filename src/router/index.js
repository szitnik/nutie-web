import Vue from 'vue'
import Router from 'vue-router'
import CoreferencesViewer from '@/components/CoreferencesViewer'
import DatasetLoader from '@/components/DatasetLoader'
import CoreferencesComparer from '@/components/CoreferencesComparer'
import CoreferenceResolution from '@/components/CoreferenceResolution'
import DatasetSplitter from '@/components/DatasetSplitter'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'DatasetLoader',
      component: DatasetLoader
    },
    {
      path: '/coreferencesViewer',
      name: 'CoreferencesViewer',
      component: CoreferencesViewer
    },
    {
      path: '/coreferencesComparer',
      name: 'CoreferencesComparer',
      component: CoreferencesComparer
    },
    {
      path: '/coreferenceResolution',
      name: 'CoreferenceResolution',
      component: CoreferenceResolution
    },
    {
      path: '/datasetSplitter',
      name: 'DatasetSplitter',
      component: DatasetSplitter
    }
  ]
})
